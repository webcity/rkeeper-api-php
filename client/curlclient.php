<?php

namespace Tech\Rkeeper\Client;

class CurlClient implements IClient
{

    private AuthClient $auth;
    public function __construct(AuthClient $authClient)
    {
        $this->auth = $authClient;
    }
    public function post(string $body): string
    {
        $crl = curl_init($this->auth->getUrl());
        curl_setopt($crl, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($crl, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($crl, CURLOPT_USERPWD, $this->auth->getAuthString());
        curl_setopt($crl, CURLOPT_POST, 1);
        curl_setopt($crl, CURLOPT_POSTFIELDS, $body);

        $data = curl_exec($crl);
        if (curl_errno($crl) > 0) {
            return curl_error($crl);
        }
        curl_close($crl);
        return $data;
    }
}