<?php

namespace Tech\Rkeeper\Client;

interface IClient
{
    public function post(string $body): string;
}