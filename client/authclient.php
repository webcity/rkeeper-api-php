<?php

namespace Tech\Rkeeper\Client;

class AuthClient
{
    private $user;
    private $password;
    private $url;

    public function __construct(string $url, string $user, string $password)
    {
        $this->user = $user;
        $this->password = $password;
        $this->url = $url;
    }
    public function getUrl(): string
    {
        return $this->url;
    }
    public function getAuthString(): string
    {
        return $this->user . ":" . $this->password;
    }
}