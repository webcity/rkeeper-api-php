<?
namespace Tech\Rkeeper\Services;

class Order extends Base
{
    public function add($orderFields): string
    {
        $xmlBody = $this->converter->setOrderParamsToCreateOrderXml($orderFields);
        $response = $this->client->post($xmlBody);
        return $this->converter->getOrderGuid($response);
    }

    public function update($guid, $orderFields): void
    {
        $xmlBody = $this->converter->setOrderParamsToUpdateOrderXml($orderFields);
        $this->client->post($xmlBody);
    }

    public function all(): string
    {
        $xmlBody = $this->converter->getOrdersXml();
        return $this->client->post($xmlBody);
    }
}