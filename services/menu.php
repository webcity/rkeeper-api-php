<?
namespace Tech\Rkeeper\Services;

class Menu extends Base
{
    public function getAll($categoryPath = ""): array
    {
        $xmlBody = $this->converter->getMenuXmlRequest($categoryPath);
        $menu = $this->client->post($xmlBody);
        return $this->converter->getMenuArrayFromXml($menu);
    }
}