<?
namespace Tech\Rkeeper\Services;
use Tech\Rkeeper\Client\IClient;
use Tech\Rkeeper\XmlConverter\XmlDomConverter;

abstract class Base
{
    protected IClient $client;
    protected XmlDomConverter $converter;
    public function __construct(IClient $client, XmlDomConverter $converter)
    {
        $this->client = $client;
        $this->converter = $converter;
    }
}