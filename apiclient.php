<?

namespace Tech\Rkeeper;

use Tech\Rkeeper\Client\Authclient;
use Tech\Rkeeper\Client\CurlClient;
use Tech\Rkeeper\Services\Menu;
use Tech\Rkeeper\Services\Order;
use Tech\Rkeeper\Client\IClient;
use Tech\Rkeeper\XmlConverter\XmlDomConverter;
class ApiClient
{
    protected XmlDomConverter $converter;
    protected IClient $client;

    public function __construct(string $user, string $password, string $url)
    {
        $authClient = new Authclient($url, $user, $password);
        $this->client = new CurlClient($authClient);

        $this->converter = new XmlDomConverter();
    }
    public function order(): Order
    {
        return new Order($this->client, $this->converter);
    }

    public function menu(): Menu
    {
        return new Menu($this->client, $this->converter);
    }
}